/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.alecgdouglas.gbenthusiast.presenter;

import android.content.res.Resources;
import android.support.v17.leanback.widget.ImageCardView;
import android.support.v17.leanback.widget.Presenter;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.alecgdouglas.gbenthusiast.R;

/*
 * A CardPresenter is used to generate Views and bind Objects to them on demand.
 * It contains an Image CardView
 */
public abstract class CardPresenter extends Presenter {
    private static int sSelectedBackgroundColor;
    private static int sDefaultBackgroundColor;
    private final CardSize mCardSize;

    CardPresenter() {
        this(CardSize.MEDIUM);
    }

    CardPresenter(CardSize cardsize) {
        mCardSize = cardsize;
    }

    private static void updateCardBackgroundColor(ImageCardView view, boolean selected) {
        int color = selected ? sSelectedBackgroundColor : sDefaultBackgroundColor;
        // Both background colors should be set because the view's background is temporarily visible
        // during animations.
        view.setBackgroundColor(color);
        view.findViewById(R.id.info_field).setBackgroundColor(color);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        sDefaultBackgroundColor = parent.getResources().getColor(R.color.default_background);
        sSelectedBackgroundColor = parent.getResources().getColor(R.color.selected_background);

        final ImageCardView cardView = new ImageCardView(parent.getContext()) {
            @Override
            public void setSelected(boolean selected) {
                updateCardBackgroundColor(this, selected);
                super.setSelected(selected);
            }
        };

        final ImageView progressOverlay = new ImageView(parent.getContext());
        progressOverlay.setImageResource(R.drawable.video_card_progress);
        progressOverlay.setScaleType(ImageView.ScaleType.FIT_CENTER);

        cardView.setFocusable(true);
        cardView.setFocusableInTouchMode(true);
        updateCardBackgroundColor(cardView, false);
        return new ViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        final ImageCardView cardView = (ImageCardView) viewHolder.view;

        final Resources res = cardView.getResources();
        cardView.setMainImageDimensions(mCardSize.getWidth(res), mCardSize.getHeight(res));

        onBindImageCardView(cardView, item);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
        final ImageCardView cardView = (ImageCardView) viewHolder.view;
        // Remove references to images so that the garbage collector can free up memory
        cardView.setBadgeImage(null);
        cardView.setMainImage(null);
    }

    abstract void onBindImageCardView(ImageCardView cardView, Object item);

    public enum CardSize {
        SMALL(R.dimen.card_width_small, R.dimen.card_height_small),
        MEDIUM(R.dimen.card_width_medium, R.dimen.card_height_medium),
        LARGE(R.dimen.card_width_large, R.dimen.card_height_large);

        private final int mWidthDimenResId;
        private final int mHeightDimenResId;

        CardSize(int widthDimenResId, int heightDimenResId) {
            mWidthDimenResId = widthDimenResId;
            mHeightDimenResId = heightDimenResId;
        }

        public int getWidth(Resources res) {
            return res.getDimensionPixelSize(mWidthDimenResId);
        }

        public int getHeight(Resources res) {
            return res.getDimensionPixelSize(mHeightDimenResId);
        }
    }
}
