/*
 * This file was copied from the Android TV Leanback sample project and modified by Alec Douglas.
 * https://github.com/googlesamples/androidtv-Leanback
 * The original file included the copyright notice below:
 *
 * Copyright (c) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.alecgdouglas.gbenthusiast.data;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.ResultReceiver;
import android.util.Log;
import android.util.Pair;

import com.alecgdouglas.gbenthusiast.ApiGetRequestParams;
import com.alecgdouglas.gbenthusiast.ApiGetRequestTask;
import com.alecgdouglas.gbenthusiast.ApiKeyManager;
import com.alecgdouglas.gbenthusiast.LastRefreshTimePrefUtils;
import com.alecgdouglas.gbenthusiast.model.Video;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * VideoFetchService is responsible for fetching the videos from the Internet and inserting the
 * results into a local SQLite database.
 */
public class VideoFetchService extends IntentService {
    public static final String EXTRA_KEY_RESULT_RECEIVER = "result_receiver";
    private static final String TAG = "VideoFetchService";
    private static final int RECEIVER_RESULT_CODE_SUCCESS = 1;
    private static final String API_VIDEOS = "videos";

    private static final String SORT_KEY = "sort";
    private static final String PUBLISH_DATE_FIELD = "publish_date";
    private static final String SORT_PUBLISH_DATE = PUBLISH_DATE_FIELD + ":desc";
    private static final String LIMIT_KEY = "limit";
    private static final int MAX_VIDEO_LIMIT = 100;
    private static final String FILTER_KEY = "filter";
    private static final String DATE_FILTER = PUBLISH_DATE_FIELD + ":%s|%s";
    private static final String OFFSET_KEY = "offset";

    private static final int INITIAL_FETCH_PAGE_COUNT = 15;
    private static final int INCREMENTAL_FETCH_PAGE_COUNT = 1;
    private static final int BACKFILL_PAGE_COUNT = 9;

    private long mStartTime = 0L;

    /**
     * Creates an IntentService with a default name for the worker thread.
     */
    public VideoFetchService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(final Intent workIntent) {
        Log.i(TAG, "Starting video fetch from server");
        mStartTime = System.nanoTime();
        final String apiKey = ApiKeyManager.getApiKey(getApplicationContext());
        if (apiKey == null) return;
        if (LastRefreshTimePrefUtils.shouldPerformFullFetch(getApplicationContext()) ||
                isVideosDbEmpty()) {
            Log.i(TAG, "Performing full refresh");
            getVideosFromServer(workIntent, apiKey, INITIAL_FETCH_PAGE_COUNT);
        } else {
            Log.i(TAG, "Performing incremental refresh");
            getVideosFromServer(workIntent, apiKey, INCREMENTAL_FETCH_PAGE_COUNT);
        }
    }

    /**
     * @param workIntent The original intent the service was started with.
     * @param apiKey     The API key to use when querying the server.
     * @param numPages   The number of pages of data we want to request.
     */
    private void getVideosFromServer(final Intent workIntent, String apiKey, int numPages) {
        final List<ApiGetRequestParams> getVideosParams = new ArrayList<>();
        final Pair<String, String> videoSortParam = new Pair<>(SORT_KEY, SORT_PUBLISH_DATE);
        final Pair<String, String> videoLimitParam =
                new Pair<>(LIMIT_KEY, Integer.toString(MAX_VIDEO_LIMIT));
        Log.i(TAG, "Requesting " + numPages + " page(s) of videos from server");
        for (int i = 0, offset = 0; i < numPages; i++, offset += MAX_VIDEO_LIMIT) {
            final Pair<String, String> offsetParam = new Pair(OFFSET_KEY, Integer.toString(offset));
            getVideosParams
                    .add(new ApiGetRequestParams(apiKey, API_VIDEOS, offsetParam, videoSortParam,
                            videoLimitParam));
        }
        if (numPages == INCREMENTAL_FETCH_PAGE_COUNT) {
            // Sneak some extra requests in there to help fill in the database with videos older
            // than what was gathered with the initial fetch.
            Log.i(TAG, "Attempting to backfill database with some older videos as well");
            final Pair<String, String> oldVideoFilterParam = getOlderVideoDateFilterParam();
            if (oldVideoFilterParam != null) {
                for (int i = 0, offset = 0; i < BACKFILL_PAGE_COUNT;
                     i++, offset += MAX_VIDEO_LIMIT) {
                    final Pair<String, String> offsetParam =
                            new Pair(OFFSET_KEY, Integer.toString(offset));
                    getVideosParams.add(new ApiGetRequestParams(apiKey, API_VIDEOS, offsetParam,
                            videoSortParam, videoLimitParam, oldVideoFilterParam));
                }

            }
        }

        final ApiGetRequestTask getVideosTask = new ApiGetRequestTask() {
            @Override
            protected void onPostExecute(Map<ApiGetRequestParams, JSONObject> responses) {
                final VideoDbBuilder builder = new VideoDbBuilder();
                final ContentResolver contentResolver =
                        getApplicationContext().getContentResolver();
                final List<ContentValues> contentValuesList = new ArrayList<>();
                for (JSONObject response : responses.values()) {
                    if (response == null) continue;
                    contentValuesList.addAll(builder.buildMedia(getApplicationContext(), response));
                }

                final ContentValues[] contentValuesArray =
                        contentValuesList.toArray(new ContentValues[contentValuesList.size()]);
                contentResolver
                        .bulkInsert(VideoContract.VideoEntry.CONTENT_URI, contentValuesArray);

                final long elapsedTimeNanos = System.nanoTime() - mStartTime;
                Log.i(TAG, "Video fetch complete! Elapsed time: " +
                        TimeUnit.NANOSECONDS.toMillis(elapsedTimeNanos) + " ms");
                Log.i(TAG, "Inserted " + contentValuesArray.length + " videos into DB");

                LastRefreshTimePrefUtils.updateLastFullFetchTime(getApplicationContext());

                final ResultReceiver receiver =
                        workIntent.getParcelableExtra(EXTRA_KEY_RESULT_RECEIVER);
                if (receiver != null) {
                    receiver.send(RECEIVER_RESULT_CODE_SUCCESS, null);
                }
            }
        };
        getVideosTask.executeInParallel(getVideosParams);
    }

    private Pair<String, String> getOlderVideoDateFilterParam() {
        final DateFormat formatter = Video.getDateFormat();
        // Start date is the cut off date for the oldest videos we want returned, which is the date
        // of the oldest video we'd want to retrieve that exists on giantbomb.com. That video is a
        // Battlefield: Bad Company review that was published on 2008-07-15 17:40:00.
        final Calendar startFilterDateCalendar = Calendar.getInstance(Locale.US);
        // Month is 0-indexed and give an extra day of leeway.
        startFilterDateCalendar.set(2008, 6, 14);
        final String startFilterDate = formatter.format(startFilterDateCalendar.getTime());
        final String endDate = getPublishDateFromDb(false);

        // If we have any videos in the database older than this date, then consider the database
        // fully backfilled. Give a bit of distance between the oldest expected video from the
        // server and the oldest date we're checking for to account for any time-zone weirdness.
        final Calendar cutOffDateCalendar = Calendar.getInstance(Locale.US);
        cutOffDateCalendar.set(2008, 6, 16);

        final Calendar endDateCalendar = Calendar.getInstance(Locale.US);
        try {
            endDateCalendar.setTime(formatter.parse(endDate));
        } catch (ParseException e) {
            Log.w(TAG, "Unable to parse the oldest publish date in the database", e);
            return null;
        }
        if (endDateCalendar.before(cutOffDateCalendar)) {
            Log.i(TAG, "Oldest video in DB is older than cut-off date for the publish date of " +
                    "the" + " " + "oldest Giant Bomb video - we have a fully backfilled database " +
                    "" + "here, " + "folks!");
            return null;
        }

        Log.i(TAG, "Requesting videos published between " + startFilterDate + " and " + endDate);
        return getEncodedDateFilterParam(startFilterDate, endDate);
    }

    private Pair<String, String> getEncodedDateFilterParam(String startDate, String endDate) {
        try {
            final String startDateEncoded =
                    URLEncoder.encode(startDate, StandardCharsets.UTF_8.name());
            final String endDateEncoded = URLEncoder.encode(endDate, StandardCharsets.UTF_8.name());
            return new Pair<>(FILTER_KEY,
                    String.format(DATE_FILTER, startDateEncoded, endDateEncoded));
        } catch (UnsupportedEncodingException e) {
            Log.w(TAG, "Unable to encode dates for filter", e);
            return null;
        }
    }

    private boolean isVideosDbEmpty() {
        final ContentResolver contentResolver = getApplicationContext().getContentResolver();
        final String[] projection = {VideoContract.VideoEntry.COLUMN_GIANT_BOMB_ID};
        try (final Cursor cursor = contentResolver
                .query(VideoContract.VideoEntry.CONTENT_URI, projection, null, null, null)) {
            if (cursor == null || cursor.getCount() == 0) {
                Log.i(TAG, "Videos database is empty");
                return true;
            }
            return false;
        }
    }

    private String getPublishDateFromDb(boolean mostRecent) {
        final ContentResolver contentResolver = getApplicationContext().getContentResolver();
        final String[] projection = {VideoContract.VideoEntry.COLUMN_PUBLISH_DATE};
        final String sortOrder = mostRecent ? "DESC" : "ASC";
        final String orderByDateTimeLimitOne = String.format("datetime(%s) %s LIMIT 1",
                VideoContract.VideoEntry.COLUMN_PUBLISH_DATE, sortOrder);

        try (final Cursor cursor = contentResolver
                .query(VideoContract.VideoEntry.CONTENT_URI, projection, null, null,
                        orderByDateTimeLimitOne)) {
            if (cursor == null || !cursor.moveToFirst()) {
                Log.d(TAG, "Unable to get " + (mostRecent ? "most recent" : "oldest") + " " +
                        "publish" + " date from DB - cursor is null or failed to move to first");
                return null;
            }
            final int publishDateIndex =
                    cursor.getColumnIndex(VideoContract.VideoEntry.COLUMN_PUBLISH_DATE);
            if (publishDateIndex < 0) {
                return null;
            }
            return cursor.getString(publishDateIndex);
        }
    }
}
