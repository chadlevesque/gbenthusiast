package com.alecgdouglas.gbenthusiast.model;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class VideoQualityTest {
    @Test
    public void testStepDownHD() {
        assertThat(VideoQuality.stepDown(VideoQuality.HD), is(VideoQuality.HIGH));
    }

    @Test
    public void testStepDownHigh() {
        assertThat(VideoQuality.stepDown(VideoQuality.HIGH), is(VideoQuality.LOW));
    }

    @Test
    public void testStepDownLow() {
        assertThat(VideoQuality.stepDown(VideoQuality.LOW), is(VideoQuality.LOW));
    }

    /**
     * Ensure that if new video qualities are added that this test will fail until it is updated.
     * Serves as a reminder to update test coverage when video qualities change.
     */
    @Test
    public void testAllVideoQualitiesCovered() {
        assertThat(VideoQuality.values().length, is(3));
    }
}
