package com.alecgdouglas.gbenthusiast.model;

import com.alecgdouglas.gbenthusiast.BuildConfig;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.Collections;
import java.util.Date;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class VideoTest {
    private static final String TEST_API_KEY = "apikey1234";
    private static final long TEST_ID = 10L;
    private static final String TEST_NAME = "Video Name";
    private static final String TEST_DECK = "This is a video deck!";
    private static final String TEST_VIDEO_TYPE_A = "Video Type A";
    private static final String TEST_VIDEO_TYPE_B = "Video Type B!";
    private static final String TEST_VIDEO_TYPE_NAME =
            TEST_VIDEO_TYPE_A + Video.VIDEO_TYPE_DELIMITER + TEST_VIDEO_TYPE_B;
    private static final Date TEST_PUBLISH_DATE = new Date();
    private static final long TEST_LENGTH_SECONDS = 100L;
    private static final String TEST_LOW_URL = "https://low.url.com/quality/vid.mp4";
    private static final String TEST_HIGH_URL = "https://high.url.com/quality/vid.mp4";
    private static final String TEST_HD_URL = "https://hd.url.com/quality/vid.mp4";
    private static final String TEST_TINY_IMAGE_URL = "http://tiny.image.url.com/quality/pic.png";
    private static final String TEST_MEDIUM_IMAGE_URL =
            "http://medium.image.url.com/quality/pic" + ".png";
    private static final String TEST_SUPER_IMAGE_URL = "http://super.image.url.com/quality/pic.png";
    private static final String TEST_ICON_IMAGE_URL = "http://icon.image.url.com/quality/pic.png";
    private static final Long TEST_POSITION_MILLIS = 50L;

    private final Video.Builder mMinimumBuilder =
            new Video.Builder(TEST_API_KEY, TEST_ID, TEST_NAME);
    private final Video.Builder mFullBuilder = new Video.Builder(TEST_API_KEY, TEST_ID, TEST_NAME);
    private Video mMinimumVideo;
    private Video mFullVideo;

    @Before
    public void setup() {
        mMinimumVideo = mMinimumBuilder.build();

        mFullBuilder.withDeck(TEST_DECK).withVideoTypeName(TEST_VIDEO_TYPE_NAME)
                .withPublishDate(TEST_PUBLISH_DATE).withLengthSeconds(TEST_LENGTH_SECONDS)
                .withLowUrl(TEST_LOW_URL).withHighUrl(TEST_HIGH_URL).withHdUrl(TEST_HD_URL)
                .withTinyImageUrl(TEST_TINY_IMAGE_URL).withMediumImageUrl(TEST_MEDIUM_IMAGE_URL)
                .withSuperImageUrl(TEST_SUPER_IMAGE_URL).withIconImageUrl(TEST_ICON_IMAGE_URL)
                .withPositionMillis(TEST_POSITION_MILLIS);
        mFullVideo = mFullBuilder.build();
    }

    @Test
    public void testMinimumBuilder() {
        assertThat(mMinimumVideo.getId(), is(TEST_ID));
        assertThat(mMinimumVideo.getName(), is(TEST_NAME));
        assertThat(mMinimumVideo.getDeck(), is(nullValue()));
        assertThat(mMinimumVideo.getVideoTypeNames(), is(Collections.<String>emptyList()));
        assertThat(mMinimumVideo.getPublishDate(), is(nullValue()));
        assertThat(mMinimumVideo.getFormattedPublishDate(), is(""));
        assertThat(mMinimumVideo.getDurationMillis(), is(0L));
        assertThat(mMinimumVideo.getFormattedDuration(), is("0s"));
        for (VideoQuality quality : VideoQuality.values()) {
            assertThat(mMinimumVideo.getVideoUrlForQuality(quality), nullValue());
        }
        assertThat(mMinimumVideo.getMediumImageUrl(), is(nullValue()));
        assertThat(mMinimumVideo.getSuperImageUrl(), is(nullValue()));
        assertThat(mMinimumVideo.getPositionMillis(), is(0L));
        assertThat(mMinimumVideo.getProgress(), is(0f));
        assertThat(mMinimumVideo.getFormattedProgressPercent(), is("0.0%"));
    }

    @Test
    public void testGetVideoTypeNamesWithNoSpaces() {
        final String videoTypeNameNoSpaces =
                TEST_VIDEO_TYPE_A + Video.VIDEO_TYPE_DELIMITER + TEST_VIDEO_TYPE_B;
        final Video video = mMinimumBuilder.withVideoTypeName(videoTypeNameNoSpaces).build();

        assertThat(video.getVideoTypeNames(), hasItems(TEST_VIDEO_TYPE_A, TEST_VIDEO_TYPE_B));
    }

    @Test
    public void testGetVideoTypeNamesWithSingleSpaces() {
        final String videoTypeNameSingleSpaces =
                TEST_VIDEO_TYPE_A + " " + Video.VIDEO_TYPE_DELIMITER + " " + TEST_VIDEO_TYPE_B;
        final Video video = mMinimumBuilder.withVideoTypeName(videoTypeNameSingleSpaces).build();

        assertThat(video.getVideoTypeNames(), hasItems(TEST_VIDEO_TYPE_A, TEST_VIDEO_TYPE_B));
    }

    @Test
    public void testGetVideoTypeNamesWithManySpaces() {
        final String videoTypeNameManySpaces =
                TEST_VIDEO_TYPE_A + "      " + Video.VIDEO_TYPE_DELIMITER + "             " +
                        TEST_VIDEO_TYPE_B;
        final Video video = mMinimumBuilder.withVideoTypeName(videoTypeNameManySpaces).build();

        assertThat(video.getVideoTypeNames(), hasItems(TEST_VIDEO_TYPE_A, TEST_VIDEO_TYPE_B));
    }

    @Test
    public void testHasUrlForQualityWithBadUrls() {
        final Video video = mFullBuilder.withHdUrl("null").withHighUrl(null).withLowUrl("").build();
        assertFalse(video.hasUrlForQuality(VideoQuality.HD));
        assertFalse(video.hasUrlForQuality(VideoQuality.HIGH));
        assertFalse(video.hasUrlForQuality(VideoQuality.LOW));
    }

    @Test
    public void testGetUrlForQualityWithBadUrls() {
        final Video video = mFullBuilder.withHdUrl("null").withHighUrl(null).build();

        // The resulting video URL will be formatted, so just check that the URL before the params
        // matches what we expect.
        assertTrue(video.getVideoUrlForQuality(VideoQuality.HD).startsWith(TEST_LOW_URL));
        assertTrue(video.getVideoUrlForQuality(VideoQuality.HIGH).startsWith(TEST_LOW_URL));
        assertTrue(video.getVideoUrlForQuality(VideoQuality.LOW).startsWith(TEST_LOW_URL));
    }

    @Test
    public void testGetUrlForQualityFindsExistingUrl() {
        // Null low and HD quality URLs so it should be forced to always return the existing high
        // quality URL - even stepping up from low to high if need be.
        final Video video = mFullBuilder.withHdUrl(null).withLowUrl(null).build();

        assertTrue(video.getVideoUrlForQuality(VideoQuality.HD).startsWith(TEST_HIGH_URL));
        assertTrue(video.getVideoUrlForQuality(VideoQuality.HIGH).startsWith(TEST_HIGH_URL));
        assertTrue(video.getVideoUrlForQuality(VideoQuality.LOW).startsWith(TEST_HIGH_URL));
    }
}
